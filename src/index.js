const express = require('express')
let app = express()
let helpers = require('./errorlog.js')
// let nunj = require('nunjucks')
const fs = require('fs')
const morgan = require('morgan')
app.use(morgan('dev'))
let path = require('path')
let bodyParser = require('body-parser')

app.use(bodyParser.json())

app.listen(8080, () => {
/* eslint-disable-next-line no-console*/
    console.log('listening on 8080')
})

// /////////// Nunjucks ////////////////////////////
// nunj.configure(['views/'], {
//     autoescape: true,
//     express: app
// })
//
// app.get('/info', (req, res) => {
//     res.render('info.njk', {appVersion: 'jsau-webserver-1.0.0'})
// })
// app.get('/nouvelles', (req, res) => {
//     res.render('index.html', {})
// })
//

////////////////////////// Logic /////////////////////////

let jsonFilePath = path.join(__dirname, '../data/nouvelles.json')


// Get all news
app.get('/nouvelles', (req, res) => {

    _getAllNews(res, (err) => helpers.handleHttpError(res, err))
})

const _getAllNews = (res, onError) => {


    fs.readFile(jsonFilePath, (err, content) => {
        let news = JSON.parse(content)

        if (!news || news.length == 0) {
            return onError(new Error('404'))
        }

        res.json({
            nouvelles:news
        })
    })


}


// Get news by id
app.get('/nouvelles/:id', (req, res) => {

    const id = parseInt(req.params.id)
    _getNewsById(id, res, ((err) => helpers.handleHttpError(res, err)))


})


const _getNewsById = (id, res, onError) => {

    if (!id) {
        return onError(new Error('404'))
    }

    fs.readFile(jsonFilePath, (err, content) => {
        let news = JSON.parse(content)

        if (!news || news.length == 0) {
            return onError(new Error('404'))
        }


        const result = news.filter((n) => n.id == id)[0]

        if (!result) {
            return onError(new Error('404'))
        }

        res.json({
            news:result,
            msg: 'GET done'
        })

    })
}


// add news
app.post('/nouvelles', (req, res) => {
    // const my_modules = require('jsau-modules')
    // my_modules.valider('toto')
    const newNews = req.body

    _postNews(newNews, res, ((err) => helpers.handleHttpError(res, err)))


})


const _postNews = (news, res, onError) => {
    if (!news) {
        return onError(new Error('400'))
    }

    // check body content
    fs.readFile(jsonFilePath, (err, content)=>{
        let oldNews = JSON.parse(content)

        // check file content
        if (!Array.isArray(oldNews)) {
            oldNews = []
        }

        let lastID = !oldNews.length ? 1 : oldNews[oldNews.length - 1]['id'] + 1

        const extNewNews = {
            id : lastID,
            ...news
        }

        const totalNews = [...oldNews, extNewNews]

        fs.writeFile(jsonFilePath, JSON.stringify(totalNews), (err) => {
            if (err) {
                throw err
            }
            res.status(201).json({
                message: 'created with id ' + lastID,

            })
        })


    })
}


// Update News
app.put('/nouvelles/:id', (req, res) => {

    const newsId = parseInt(req.params.id)
    const newNews = req.body
    _updateNews(newsId, newNews, res, ((err) => helpers.handleHttpError(res, err)))


})


const _updateNews = (id, content, res, onError) => {

    if (!id || !content) {
        throw new Error('400')
    }


    fs.readFile(jsonFilePath, (err, file)=>{
        let oldNews = JSON.parse(file)

        // check file content
        if (!Array.isArray(oldNews)) {
            oldNews = []
        }

        let newsExists = false

        const newNews = oldNews.map(
            (news) => {
                if (news.id == id) {
                    newsExists = true
                    return {
                        id,
                        ...content
                    }
                }
                return news
            })

        if (!newsExists) {
            return onError(new Error('404'))
        }

        fs.writeFile(jsonFilePath, JSON.stringify(newNews), (err) => {
            if (err) {
                throw err
            }

            res.json({
                msg: 'Updated'
            })


        })

    })
}


// Delete News
app.delete('/nouvelles/:id', (req, res) => {

    const newsId = parseInt(req.params.id)


    _deleteNews(newsId, res, ((err) => helpers.handleHttpError(res, err)))


})


const _deleteNews = (id, res, onError) => {

    if (!id) {
        return onError(new Error('400'))
    }

    fs.readFile(jsonFilePath, (err, content) => {
        let oldNews = JSON.parse(content)

        // check file content
        if (!Array.isArray(oldNews)) {
            oldNews = []
        }


        const index = oldNews.findIndex((news) => news.id == id)

        if (0 > index) {
            return onError(new Error('404'))
        }


        // delete 1 item at index
        oldNews.splice(index, 1)

        fs.writeFile(jsonFilePath, JSON.stringify(oldNews), (err) => {

            if (err) {
                return onError(err)

            }
            res.json({
                msg : 'deleted'
            })
        })
    })
}
