module.exports.handleHttpError = (response, err) => {
    switch (err.message) {
    case '404':
        response.status(404).json({
            error : 'Not Found'
        })
        break
    case '400':
        response.status(400).json({
            error : 'Bad Input'
        })
        break
    default:
    /* eslint-disable-next-line no-console*/
        console.log(err)
        response.status(500).json({
            error : err
        })

    }
}
module.exports.succesHttpResponseView = (response, message = null, redirectTo = '/news', status = 200) => {
    response.redirect(redirectTo)
}


module.exports.succesHttpResponse = (response, result, status = 200) => {
    response.status(status).json({
        ...result
    })
}
